# Unofficial: フロントエンド

コンピューターフェスティバル2020 AP-04

## 起動方法

### 開発

```
yarn serve
```

### 開発(モックサーバー)

```
yarn mock
```

### ビルド

```
yarn build
```
