import axios from 'axios';
import mock from '@/mock/api';

export const client = axios.create({
  baseURL: (process.env.NODE_ENV === 'production')
      ? 'https://unofficial-api.herokuapp.com/api'
      : 'http://localhost:8080/api'
});

// モック起動
if(process.env.VUE_APP_USE_MOCK === 'true') {
  mock.run(client)
}

export default {
  /**
   * 連絡事項取得 API
   * 結果は Promise を返してハンドリングする
   * @param {Object} params パラメータ
   */
  getCanceledClasses(params) {
    return client.get('/classes/canceled/', params);
  },

  getMovedClasses(params) {
    return client.get('/classes/moved/', params);
  },

  getSupplementaryClasses(params) {
    return client.get('/classes/supplementary/', params);
  }
};
