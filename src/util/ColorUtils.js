'use strict';

export default {
  getProgramColor(program) {
    /* 系(組)の色を取得 */
    if(program === 1 || program === 'S') {
      return 'var(--s-senshin)';

    } else if(program === 2 || program === 'M') {
      return 'var(--m-kikai)';

    } else if(program === 3 || program === 'E') {
      return 'var(--e-denkidenshi)';

    } else if(program === 4 || program === 'C') {
      return 'var(--c-joho)';

    } else {
      return '#cccccc';

    }
  }
}
