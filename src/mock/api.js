import MockAdapter from 'axios-mock-adapter';

const canceledMockData = [
  {
    "id": "1",
    "className": "制御工学",
    "periods": [
      1,
      2
    ],
    "date": "2020-02-06",
    "teacher": "曽利",
    "note": "",
    "classNumber": {
      "grade": 4,
      "program": "C"
    }
  },
  {
    "id": "2",
    "className": "in incididunt laborum anim",
    "periods": [
      1, 2
    ],
    "date": "1976-12-30",
    "teacher": "cupidatat officia",
    "note": "dolore",
    "classNumber": {
      "grade": 3,
      "program": "M"
    }
  },
  {
    "id": "3",
    "className": "eiusmod incididunt amet",
    "periods": [
      1, 2
    ],
    "date": "1981-08-25",
    "teacher": "enim fugiat deserunt",
    "note": "culpa Lorem exercitation",
    "classNumber": {
      "grade": 4,
      "program": "E"
    }
  },
  {
    "id": "4",
    "className": "Ut labore qui deserunt cillum",
    "periods": [
      1, 2
    ],
    "date": "1967-08-25",
    "teacher": "magna esse laborum",
    "note": "culpa sunt occaecat in labore",
    "classNumber": {
      "grade": 5,
      "program": "S"
    }
  }
];

const movedMockData = [
  {
    "id": "1",
    "before": {
      "className": "情報システム工学実験実習",
      "date": "2018-06-05",
      "periods": [
        1,
        2
      ],
      "note": "",
      "teacher": "宮下"
    },
    "after": {
      "className": "CAD入門",
      "date": "2018-06-05",
      "periods": [
        1,
        2
      ],
      "note": "",
      "teacher": "MTMT"
    },
    "classNumber": {
      "grade": 3,
      "program": "C",
      "formerClass": false
    }
  },
  {
    "id": "2",
    "before": {
      "className": "微分積分II",
      "date": "1984-02-18",
      "periods": [
        3, 4
      ],
      "note": "皆終わるまで帰れません",
      "teacher": "横谷"
    },
    "after": {
      "className": "CAD入門",
      "date": "1984-02-18",
      "periods": [
        3, 4
      ],
      "note": "コラァ〜！",
      "teacher": "MTMT"
    },
    "classNumber": {
      "grade": 3,
      "program": "C",
      "formerClass": false
    }
  }
];

export default {
  run: client => {
    const mock = new MockAdapter(client);

    mock.onGet('/classes/canceled/').reply(200, canceledMockData);
    mock.onGet('/classes/moved/').reply(200, movedMockData);
    mock.onGet('/classes/supplementary/').reply(200, canceledMockData);
  }
}